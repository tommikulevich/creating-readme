# Palletizing (PTZ)

## 📑 Design Assumptions
The aim of the project is to prepare a robot working environment comprising a transmission belt with objects, a pallet and a mobile robot equipped with a gripper arm. The robot should place objects on the pallet so as to make maximum use of it.

## 💻 Tech Stack
1. **Unity** `2021.3.19f1`
2. **C#**
3. **Python**

## 💡 Final Requirements
- Model of mobile robot equipped with six-axis arm, vacuum gripper, noisy sensors (rgbd camera, encoders)
- An animated environment including palette, transmission belt and moving robot
- Randomly generated boxes with different sizes (50mm - 600mm in every dimension) and weights (1kg - 20kg)
- Robot should move into transmission belt, pick up box, transport it to the palette and put it down
- Task of robot is to make maximum use of the 1200 x 800 x 800 palette with a weight limit of 1500 kg
- GUI with possibility to choose the size of the palette and display the list of objects on the palette

## 📶 Milestones:
**#1:**
- Model of mobile robot equipped with six-axis arm, vacuum gripper
- An animated environment including palette, transmission belt and moving robot
- Randomly generated boxes with different sizes (50mm - 600mm in every dimension) and weights (1kg - 20kg)
- Properly working inverse kinematics

**#2:**
- Robot interacts with boxes and palette
- Getting information from sensors and filtering noises
- GUI (displaying informations about environment, prepared buttons for future features)

**#3:**
- Palletizing algorithm
- Opportunity to change the size of palette
- List of objects arranged in a specific order

**Deadline:**
- Complete documentation
- Fully integrated environment 
- Small changes (aesthetic changes, optimization)
- Installer

## 📅 Timetable
| Date | Goals |
| :--- | :---  | 
| **27.02.2023**       | Create design assumptions, timetable and requirements   |
| **06.03.2023**      | Introduction to Unity, first models   | 
| **13.03.2023** | Improve models, theoretical inverse kinematics | 
| **20.03.2023** | Implementation of inverse kinematics, animation and physics | 
| **27.03.2023** | Merge all elements into one environment | 
| **<ins>03.04.2023</ins>** | Fix errors, research about sensors, filtering noises  | 
| **10.04.2023** | First implementation of sensors and filtering, simple GUI | 
| **17.04.2023** | Continuation of working on sensors, filtering and GUI | 
| **24.04.2023** | Completion of working on sensors, filtering | 
| **01.05.2023** | Interaction robot with objects | 
| **<ins>08.05.2023</ins>** | Fix errors, research about palletizing algorithm | 
| **15.05.2023** | GUI updates, start of work on the algorithm | 
| **22.05.2023** | Finished GUI, finished palletizing algorithm | 
| **29.05.2023** | Tests and debug | 
| **<ins>05.06.2023</ins>** | Fix errors, start of the process of completing documentation | 
| **12.06.2023** | Completed project with full documentation and prepared installer | 

## 💪 Team
- **Szymon Kryzel**
- **Alan Bejnarowicz**
- Paweł Drożek
- Julia Strasser
- Michał Kaczmarek
- Paweł Okuński
- Tomash Mikulevich
- Radosław Dębiński
- Bartosz Janicki
- Maciej Rogowicz
